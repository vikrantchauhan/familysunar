
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant extends CI_Controller {

    function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
                
    }
    
    /**@desc Loads the default view for merchant section.
      */ 
    function index(){
        $this->load->view('merchant/header');
        $this->load->view('merchant/collapse');
        $this->load->view('merchant/welcome_banner');
        $this->load->view('merchant/navigation'); 
        $this->load->view('merchant/maincontent'); 
        $this->load->view('merchant/footer');
    }
    
    /**@desc First view for Registration i.e Mobile number.
      */
    function register1(){
        $this->load->view('merchant/reg-header');
        $this->load->view('merchant/register-1');
        $this->load->view('merchant/footer');
    }
    
    /**@desc Second View for Registration i.e OTP .
      */ 
    function register2(){
        $this->load->view('merchant/reg-header');
        $this->load->view('merchant/register-2');
        $this->load->view('merchant/footer');
    }
    
    /**@desc Third View for Registration i.e Complete Information like PAN and VAT Id. .
      */ 
    function register3(){
        $this->load->view('merchant/reg-header');
        $this->load->view('merchant/register-3');
        $this->load->view('merchant/footer');
    }
    
    
}