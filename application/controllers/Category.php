
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
                
    }
     
    
    function index(){
    	$this->load->view('header');
        $this->load->view('welcome_banner');
        $this->load->view('navigation');
        $this->load->view('category');
        $this->load->view('footer');
        
    }
}