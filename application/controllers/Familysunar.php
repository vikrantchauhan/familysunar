
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Familysunar extends CI_Controller {

    function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
                
    }
     /**@desc Home-page for costomer section.
      */ 
    function index(){
    	$this->load->view('user/header');
        $this->load->view('user/collapse');
        $this->load->view('user/welcome_banner');
        $this->load->view('user/navigation');
        $this->load->view('user/product');
        $this->load->view('user/footer');
        
    }
    
   /**@desc Loads view for Category of Products.
      */
    function category(){
    	$this->load->view('user/header');
        $this->load->view('user/collapse');
        $this->load->view('user/navigation');
        $this->load->view('user/category');
        $this->load->view('user/footer');
        
    }
    
    /**@desc Loads view for cartpage.
      */ 
    function cartpage(){
    	$this->load->view('user/header');
        $this->load->view('user/collapse');
        $this->load->view('user/navigation');
        $this->load->view('user/cart-page');
        $this->load->view('user/footer');
        
    }
    
    /**@desc Loads view for Detailpage of a product.
      */ 
    function detailpage(){
    	$this->load->view('user/header');
        $this->load->view('user/collapse');
        $this->load->view('user/navigation');
        $this->load->view('user/detail');
        $this->load->view('user/footer');
        
    }   
    
    function register(){
    	$this->load->view('user/reg-header');
        $this->load->view('user/collapse');
        $this->load->view('user/user_register');
        $this->load->view('user/reg-footer');
        
    }
    
    function registerOTP(){
    	$this->load->view('user/header');
        $this->load->view('user/collapse');
        $this->load->view('user/user_otp');
        $this->load->view('user/footer');
        
    }
    
    function login(){
        $this->load->view('user/header');
        $this->load->view('user/collapse');
        $this->load->view('user/user_login');
        $this->load->view('user/footer');
    }
    
    function profile_view(){
        $this->load->view('user/dashboard/header');
        $this->load->view('user/navigation');
        $this->load->view('user/dashboard/welcome_page');
        $this->load->view('user/footer');
    }
    
    function edit_profile(){
        $this->load->view('user/dashboard/header');
        $this->load->view('user/navigation');
        $this->load->view('user/dashboard/edit_profile');
        $this->load->view('user/footer');
    }
    
    function order_history(){
        $this->load->view('user/dashboard/header');
        $this->load->view('user/navigation');
        $this->load->view('user/dashboard/order_history');
        $this->load->view('user/footer');
    }
    
    function refer_friend(){
        $this->load->view('user/dashboard/header');
        $this->load->view('user/navigation');
        $this->load->view('user/dashboard/refer-friend');
        $this->load->view('user/footer');
    }
  
}
    