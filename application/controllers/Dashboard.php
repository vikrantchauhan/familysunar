<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __Construct(){
        parent::__Construct();
        $this->load->helper('url');
                
    }
     
    /**@desc Dashboard main view.
      */
    function index(){
    	$this->load->view('dashboard/main-header');
        $this->load->view('dashboard/navigation');
        $this->load->view('dashboard/listing');    
    }
    
    /**@desc Loads lists of listing view.
      */
     function lists(){
    	$this->load->view('dashboard/main-header');
        $this->load->view('dashboard/navigation');
        $this->load->view('dashboard/lists');    
    }
    
    /**@desc Loads view for Order-Section.
      */
     function order_section(){
    	$this->load->view('dashboard/main-header');
        $this->load->view('dashboard/navigation');
        $this->load->view('dashboard/order-section');    
    }
    
    /**@desc Loads view for Other-views.
      */
     function other_reports(){
    	$this->load->view('dashboard/main-header');
        $this->load->view('dashboard/other-reports');    
    }
}