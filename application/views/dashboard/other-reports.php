<!-- stats information container ends-->

        <div class="container_dash">

            <div class="dash_inner">

                <div class="dash_ul">

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/revenue.png">
                        <span class="dash_li_sp_1">Revenue</span>
                        <span class="dash_li_sp_2">Rs, 100,000</span>
                    </div>

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/total-order.png">
                        <span class="dash_li_sp_1">Total Orders</span>
                        <span class="dash_li_sp_2">25</span>
                    </div>

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/unique-coustumer.png">
                        <span class="dash_li_sp_1">Total Unique Customers</span>
                        <span class="dash_li_sp_2">100</span>
                    </div>

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/tick.png">
                        <span class="dash_li_sp_1">Orders to Approve</span>
                        <span class="dash_li_sp_2">50</span>
                    </div>

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/rating.png">
                        <span class="dash_li_sp_1">Customer Rating</span>
                        <span class="dash_li_sp_2">4.5</span>
                    </div>

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/payment-due.png">
                        <span class="dash_li_sp_1">Total Payment Due</span>
                        <span class="dash_li_sp_2">Rs, 10,0000</span>
                    </div>

                    <div class="dash_li">
                        <img src="<?php echo base_url(); ?>images/last-date-of-payment.png">
                        <span class="dash_li_sp_1">Last Date of Payement</span>
                        <span class="dash_li_sp_2">12-02-2016</span>
                    </div>

                </div>


                <!-- stats information container ends-->


                <!-- begining of tabs -->


                <div class = "dash_ul ul_4">

                    <div class="dash_li1">
                        
			<div>
                            <a href="#" class="tablinks" onclick="openTab(event, 'Seller')">New Listed Product Of Seller</a>
                        </div>

                    </div>

                    <div class="dash_li1">
    
                        <div>
                            <a href="#" class="tablinks" onclick="openTab(event, 'Inventory')">Low Inventry Products</a>
                        </div>

                    </div>

                    <div class="dash_li1">    

                        <div>
			    <a href="#" class="tablinks" onclick="openTab(event, 'Notification')">Notifications</a>
                        </div>

                    </div>

                </div>    

                <!-- ending of tabs -->



                <!--Detailed information under each tab-->


                <div id="Seller" class="tabcontent">  
                    
                    <div class="tabcontent1">

                        <div class = "dashboard1">
                            <span>Product Image</span>
                        </div>    

                        <div class = "dashboard1">
                            <span>SKU Code</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Product Name</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Product Line 1</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Inventory</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Customer Rating</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Metal Type</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Discount %</span>
                        </div>

                    </div>

                    <div class = "tabcontent2">
                
                        <div class = "dashboard1 dashboard_img">
                            <img src = "<?php echo base_url(); ?>images/bengal.gif">
                        </div>
    
                        <div class = "dashboard1">
                            <span>020002</span>
                        </div>

                        <div class = "dashboard1">
                            <span>White Bangels</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Priduct Line 1</span>
                        </div>

                        <div class = "dashboard1">
                            <span>123</span>
                        </div>

                        <div class = "dashboard1">
                            <span>4.5</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Gold</span>
                        </div>

                        <div class = "dashboard1">
                            <span>10%</span>
                        </div>
                        
                    </div>

                </div> 


                <div id="Inventory" class="tabcontent">
  
                    <div class="tabcontent1">
                        <div class = "dashboard1">
                            <span>Product Image</span>
                        </div>
    
                        <div class = "dashboard1">
                            <span>SKU Code</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Product Name</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Product Line 1</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Inventory</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Customer Rating</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Metal Type</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Discount %</span>
                        </div>

                    </div>

                    <div class = "tabcontent2">
                  
                        <div class = "dashboard1 dashboard_img">
                            <img src = "<?php echo base_url(); ?>images/bengal.gif">
                        </div>
    
                        <div class = "dashboard1">
                            <span>020002</span>
                        </div>

                        <div class = "dashboard1">
                            <span>White Bangels</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Priduct Line 1</span>
                        </div>

                        <div class = "dashboard1">
                            <span>123</span>
                        </div>

                        <div class = "dashboard1">
                            <span>4.5</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Gold</span>
                        </div>

                        <div class = "dashboard1">
                            <span>10%</span>
                        </div>
   
                    </div>

                </div> 
                    
              
                <div id="Notification" class="tabcontent">  

                    <div class="tabcontent1">

                        <div class = "dashboard1">
                            <span>Product Image</span>
                        </div>
    
                        <div class = "dashboard1">
                            <span>SKU Code</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Product Name</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Product Line 1</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Inventory</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Customer Rating</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Metal Type</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Discount %</span>
                        </div>

                    </div>

                    <div class = "tabcontent2">
                
                        <div class = "dashboard1 dashboard_img">
                            <img src = "<?php echo base_url(); ?>images/bengal.gif">
                        </div>
    
                        <div class = "dashboard1">
                            <span>020002</span>
                        </div>

                        <div class = "dashboard1">
                            <span>White Bangels</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Priduct Line 1</span>
                        </div>

                        <div class = "dashboard1">
                            <span>123</span>
                        </div>

                        <div class = "dashboard1">
                            <span>4.5</span>
                        </div>

                        <div class = "dashboard1">
                            <span>Gold</span>
                        </div>

                        <div class = "dashboard1">
                            <span>10%</span>
                        </div>
   
                    </div>

                </div>  

                

            </div>    
             
        </div>    

                    <!--Detailed information under each tab ends-->

                    <!--JavaScript begins-->

        <script>
            function openTab(evt, tabName) {

                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("tabcontent");

                for (i = 0; i < tabcontent.length; i++) {

                    tabcontent[i].style.display = "none";
                }

                tablinks = document.getElementsByClassName("tablinks");

                for (i = 0; i < tablinks.length; i++) {

                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }

                document.getElementById(tabName).style.display = "block";
                evt.currentTarget.className += " active";
                
           }
        </script>

                <!--JavaScript ends-->

    </body>    

</html>
