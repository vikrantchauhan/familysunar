<div class="container_dash">
    <div class="dash_inner">
        
        <div class="dash_ul ul_1">
            
            <div class="dash_li">
            
            <img src="<?php echo base_url(); ?>d_images/revenue.png">
            
            <span class="dash_li_sp_1">Revenue</span>
            
            <span class="dash_li_sp_2">Rs, 100,000</span>
        
        </div>
        
        <div class="dash_li">
        
            <img src="<?php echo base_url(); ?>d_images/total-order.png">
        
            <span class="dash_li_sp_1">Total Orders</span>
        
            <span class="dash_li_sp_2">25</span>
        
        </div>
        
        <div class="dash_li">
        
            <img src="<?php echo base_url(); ?>d_images/unique-coustumer.png">
        
            <span class="dash_li_sp_1">Total Unique Customers</span>
        
            <span class="dash_li_sp_2">100</span>
        
        </div>
        
        <div class="dash_li">
        
            <img src="<?php echo base_url(); ?>d_images/tick.png">
        
            <span class="dash_li_sp_1">Orders to Approve</span>
        
            <span class="dash_li_sp_2">50</span>
        
        </div>
        
        <div class="dash_li">
        
            <img src="<?php echo base_url(); ?>d_images/rating.png">
        
            <span class="dash_li_sp_1">Customer Rating</span>
        
            <span class="dash_li_sp_2">4.5</span>
        
        </div>
        
        <div class="dash_li">
        
            <img src="<?php echo base_url(); ?>d_images/payment-due.png">      
            <span class="dash_li_sp_1">Total Payment Due</span>
            <span class="dash_li_sp_2">Rs, 10,0000</span>
        </div>
        
        <div class="dash_li">
        
            <img src="<?php echo base_url(); ?>d_images/last-date-of-payment.png">        
            <span class="dash_li_sp_1">Last Date of Payement</span>
            <span class="dash_li_sp_2">12-02-2016</span>        
        </div>
    
    </div>
</div>
