<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="height=device-height, initial-scale=1.0">
        <title>Home Page</title>
        
        <link href="<?php echo base_url(); ?>css/other_report.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/dashboard.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/listing_page_style.css" type="text/css" rel="stylesheet">
        <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" type="text/css" rel="stylesheet"/>
	    <link href="<?php echo base_url(); ?>css/autocomplete_style.css" rel="stylesheet">
        
        <script src="<?php echo base_url(); ?>js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/demo.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/listing_page_script.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
	    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	    <script src="<?php echo base_url(); ?>js/autocomplete.multiselect.js"></script>

    </head>

    <body>

        <header class="fs_or_header">

            <div class="logo_or">
                <img class="fs_or_logo" src="<?php echo base_url(); ?>images/logo.png">
            </div>

            <div class="fs_or_nav">

                <a href="<?php echo base_url(); ?>dashboard">

                    <span class="fs_or_a active_dash_li_1">
                    
                        <img src="<?php echo base_url(); ?>d_images/listing.gif">
                    
                        <span class="fs_or_s1"> Listing </span>
                    
                    </span>
                
                </a>

                <a href="<?php echo base_url(); ?>dashboard/order_section">

                    <span class="fs_or_a ">

                        <img src="<?php echo base_url(); ?>d_images/order-section.gif">
                
                        <span class="fs_or_s1">Order Section</span> 
                
                    </span>
                </a>

                <a href="<?php echo base_url(); ?>dashboard/other_reports">
                   
                    <span class="fs_or_a">
                        
                        <img src="<?php echo base_url(); ?>d_images/other-report.gif">
                        
                        <span class="fs_or_s1">Other Report</span>
                    
                    </span>
                
                </a>
                
                <a href="">
                
                    <span class="fs_or_a">
                
                        <img src="<?php echo base_url(); ?>d_images/payment-info.gif">
                
                        <span class="fs_or_s1">Payment Info</span>
                
                    </span>
                
                </a>
                
                <a href="">
                
                    <span class="fs_or_a">
                
                        <img src="<?php echo base_url(); ?>d_images/sale-sumary.gif">
                
                        <span class="fs_or_s1">Sale Summary</span>
                
                    </span>
                
                </a>

            </div>

            <div class="search_fs">

                <span class="search_fs_1">

                    <img src="<?php echo base_url(); ?>d_images/phone.gif">

                    <span>Merchant Support Number - 1800800500</span>

                </span>

                <div class="search_fs_2">

                    <input type="text" name="search" placeholder="Search">

                </div>

            </div>

            <div class="extra_fs">

                <img class="extra_fs_1" src="<?php echo base_url(); ?>d_images/profile.gif">
                <img class="extra_fs_2" src="<?php echo base_url(); ?>d_images/notification.gif">

            </div>

        </header>
