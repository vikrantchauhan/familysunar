<!-- *********************  main-container  **************************** -->
 	<div class="main__listing_container">
 		
 		<h3 class="product-listing">Product Listing</h3>

 		<h4 class="all-images">All Images</h4>

<!-- *********************  slider image container *********************** -->
 		<div class="slide-img-container">
 			
 			<div class="img-box left-arrow-box" id="left-arrrow">
 				<a href="#"><img src="<?php echo base_url(); ?>images/arrow.png"></a>
 			</div>
 			
 			<div class=" img-box show-image">
 				<a href="#"><img src="<?php echo base_url(); ?>images/image.png"></a>
 					<div class="img-btn">
 						<button class="hide-btn view"><img src="<?php echo base_url(); ?>images/eye.png">view</button>
 						<button class="hide-btn delete"><img src="<?php echo base_url(); ?>images/del-btn.png">delete</button>
 					</div>
 			</div>

 			<div class="img-box show-image">
 				<a href="#"><img src="<?php echo base_url(); ?>images/image.png"></a>
 					<div class="img-btn">
 						<button class="hide-btn view"><img src="<?php echo base_url(); ?>images/eye.png">view</button>
 						<button class="hide-btn delete"><img src="<?php echo base_url(); ?>images/del-btn.png">delete</button>
 					</div>
 			</div>

 			<div class="img-box show-image">
 				<a href="#"><img src="<?php echo base_url(); ?>images/image.png"></a>
 					<div class="img-btn">
 						<button class="hide-btn view"><img src="<?php echo base_url(); ?>images/eye.png">view</button>
 						<button class="hide-btn delete"><img src="<?php echo base_url(); ?>images/del-btn.png">delete</button>
 					</div>
 			</div>

 			<div class="img-box show-image">
 				<a href="#"><img src="<?php echo base_url(); ?>images/image.png"></a>
 					<div class="img-btn">
 						<button class="hide-btn view" id="view-btn"><img src="<?php echo base_url(); ?>images/eye.png">view</button>
 						<button class="hide-btn delete" id="delete-btn"><img src="<?php echo base_url(); ?>images/del-btn.png">delete</button>
 					</div>
 			</div>

 			<div class="img-box show-image" id="plus-img">
 				<a href="#"><img src="<?php echo base_url(); ?>images/plus.png"></a>
 			</div>

 			<div class="img-box right-arrow-box" id="right-arrrow">
 				<a href="#"><img src="<?php echo base_url(); ?>images/arrow-2.png"></a>
 			</div>


 		</div>
<!--  *********************** End of slider image container ***************** -->

<!-- ************************ Start of form field *********************************** -->

 		<div class="input-field">
 			
 			<div class="row">
 				
 				<div class="left-field sku-code-field small-field">
 					<label for="sku-code">SKU code</label><br>
 					<input type="text" name="sku" placeholder="Sku Code" id="sku-code">
 				</div>

 				<div class="right-field metal-type-field">
 					<label for="metal-type">Metal Type</label><br>
 					<select class="dropdown-field metal-dropdown">
 						<option value="a">Metal Type</option>
 						<option value="b">b</option>
 						<option value="c">c</option>
 						<option value="d">d</option>
 					</select>
 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="left-field product-name-field small-field" >
 					<label for="product-name">Product name</label><br>
 					<input type="text" name="productname" placeholder="Product name" id="product-name">
 				</div>

 				<div class="right-field inventry-field small-field">
 					<label for="inventry">Inventry"</label><br>
 					<input type="text" name="inventry" placeholder="Inventry" id="invebtry">
 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="product-line-1-field large-field">
 					<label for="product-name">Product Line 1</label><br>
 					<input type="text" name="productline1" placeholder="Product line 1" id="product-line1">
 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="product-line-2-field large-field">
 					<label for="product-name">Product Line 2</label><br>
 					<input type="text" name="productline2" placeholder="Product line 2" id="product-line2">
 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="left-field gold-weight-field small-field">
 					<label for="gold-weight">Gold Weight in GM</label><br>
 					<input type="text" name="goldweight" placeholder="Gold Weight" id="gold-weight">
 				</div>

 				<div class="right-field gold-purity-field">
 					<label for="gold-purity">Gold Purity</label><br>
 					<select class="dropdown-field gold-purity-dropdown">
 						<option value="22pt">22pt</option>
 						<option value="23">b</option>
 						<option value="24">c</option>
 						<option value="25">d</option>
 					</select>
 				</div>

 			</div>

 			<div class="add-extraa-field">
 				
 				<div class="btn-field-box">
 					<button class="add-field-btn"><img src="<?php echo base_url(); ?>images/plus_icon.png"></button>
 				</div>
 				<div class="extraa-field">
 					
 					<div class="row">
 						
 						<div class="left-field diamond-cut-field">
 							<label for="diamond-cut">Diamond cut</label><br>
 							<select class="dropdown-field diamond-cut-dropdown back-color">
 								<option value="22pt">cut</option>
 								<option value="23">b</option>
 								<option value="24">c</option>
 								<option value="25">d</option>
 							</select>
 						</div>

 						<div class="right-field diamond-clarity-field">
 							<label for="diamond-cut">Diamond clarity</label><br>
 							<select class="dropdown-field diamond-clarity-dropdown back-color">
 								<option value="22pt">clarity</option>
 								<option value="23">b</option>
 								<option value="24">c</option>
 								<option value="25">d</option>
 							</select>
 						</div>
 					
 					</div>

 					<div class="row">
 						
 						<div class="left-field diamond-quantity-field small-field">
 							<label for="diamond-quantity">No. of Diamonds</label><br>
 							<input type="text" name="diamondsquantity" placeholder="222" id="diamonds-quantity">
 						</div>

 						<div class="right-field diamond-shape-field">
 							<label for="diamond-shape">Diamond shape</label><br>
 							<select class="dropdown-field diamond-shape-dropdown back-color">
 								<option value="22pt">tringle</option>
 								<option value="23">b</option>
 								<option value="24">c</option>
 								<option value="25">d</option>
 							</select>
 						</div>
 					
 					</div>

 					<div class="row">
 						
 						<div class="left-field diamond-weight-field small-field">
 							<label for="diamond-weight">Diamonds Weight</label><br>
 							<input type="text" name="diamondsweight" placeholder="Diamonds Weight" id="diamonds-weight">
 						</div>

 						<div class="right-field diamond-setting-type-field">
 							<label for="diamond-setting-shape">Diamond Setting Type</label><br>
 							<select class="dropdown-field diamond-seting-type-dropdown back-color">
 								<option value="22pt">type</option>
 								<option value="23">b</option>
 								<option value="24">c</option>
 								<option value="25">d</option>
 							</select>
 						</div>
 					
 					</div>

 				</div>
 			</div>

 			<div class="add-field"></div>

 			<div class="certificate">
 				<p class="pan-card"> Diamond Certificate </p>
                <button class="certificate-btn" id="upload-pan-card"><img src="<?php echo base_url(); ?>d_images/upload.png">Upload Pan Card Image</button>
 			</div>

 			<div class="add-extraa-field">
 				
 				<div class="btn-field-box">
 					<button class="add-field-btn"><img src="<?php echo base_url(); ?>images/plus_icon.png"></button>
 				</div>

 				<div class="row">
 				
	 				<div class="left-field other-name-field small-field" >
	 					<label for="other-name">Other Name</label><br>
	 					<input type="text" name="othername" placeholder="Other name" id="other-name">
	 				</div>

	 				<div class="right-field other-weight-field small-field">
	 					<label for="other-weight">Other Weight"</label><br>
	 					<input type="text" name="otherweight" placeholder="Other-weight" id="other-weight">
	 				</div>

 				</div>

 				<div class="row">
 				
	 				<div class="left-field other-Value-field small-field" >
	 					<label for="other-Value">Other Value</label><br>
	 					<input type="text" name="othervalue" placeholder="Other value" id="other-value">
	 				</div>

 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="left-field length-field small-field" >
 					<label for="length">Length</label><br>
 					<input type="text" name="length" placeholder="Leagth" id="length">
 				</div>

 				<div class="right-field width-field small-field">
 					<label for="width">Width</label><br>
 					<input type="text" name="width" placeholder="width" id="width">
 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="left-field  small-field" >
 					<label for="diameter">Diameter</label><br>
 					<input type="text" name="diameter" placeholder="Diameter" id="diameter">
 				</div>

 				<div class="right-field small-field">
 					<label for="making-charges">Making charges</label><br>
 					<input type="text" name="makingcharges" placeholder="Making Charges" id="making-charges">
 				</div>

 			</div>


 			<div class="row">
 				
 				<div class="left-field  small-field" >
 					<label for="discount-type">Discount Type</label><br>
 					<input type="text" name="discounttype" placeholder="State" id="discount-type">
 				</div>

 				<div class="right-field  small-field">
 					<label for="discount-line">Discount Line</label><br>
 					<input type="text" name="discountline" placeholder="Discount Line" id="discount-line">
 				</div>

 			</div>

 			<div class="row">
 				
 				<div class="left-field  small-field" >
 					<label for="discount-percentage">Discount%</label><br>
 					<input type="text" name="discountpercentage" placeholder="Discount" id="discount-percentage">
 				</div>

 				<div class="right-field  small-field">
 					<label for="customer-rating">Customer Rating</label><br>
 					<input type="text" name="customerrating" placeholder="222" id="customer-rating">
 				</div>

 			</div>
                        

     <!-- *********************** keyword box ******************* -->
 			<!-- <div class="row keyword-box">
 				<p class="pan-card">Keywords</p>
 				<dl class="dropdown"> 
  
				    <dt>
				    <a href="#">
				      <span class="hida">Select</span>    
				      <p class="multiSel"></p>  
				    </a>
				    </dt>
				  
				    <dd>
				        <div class="mutliSelect">
				            <ul>
				                <li>
				                    <input type="checkbox" value="Apple" />Apple</li>
				                <li>
				                    <input type="checkbox" value="Blackberry" />Blackberry</li>
				                <li>
				                    <input type="checkbox" value="HTC" />HTC</li>
				                <li>
				                    <input type="checkbox" value="Sony Ericson" />Sony Ericson</li>
				                <li>
				                    <input type="checkbox" value="Motorola" />Motorola</li>
				                <li>
				                    <input type="checkbox" value="Nokia" />Nokia</li>
				            </ul>
				        </div>
				    </dd>
				</dl>
 			</div> -->
 			
 			
 			<div class="main-div row">
				
				<p class="pan-card">Keywords</p>
				<input id="myAutocompleteMultiple" type="text" />
			
			</div>

 			
    <!-- *********************** End of keyword box ******************* -->
 			
    <!-- *********************** button container ******************* -->
 			<div class="row">

 				<div class="action-btn">
	 				<button class="inline-btn" id="save-btn"><img src="<?php echo base_url(); ?>images/save_icon.png">Save</button>
	 				<button class="inline-btn" id="clear-btn"><img src="<?php echo base_url(); ?>images/clear_icon.png">Clear</button>
	 				<button class="inline-btn" id="cancel-btn"><img src="<?php echo base_url(); ?>images/cancel_btn.png">Cancel</button>
 				</div>
 				
 			</div>

    <!-- *********************** End of button container ******************* -->
 		</div>

<!-- ************************ Start of form field *********************************** --> 		
 	</div>
 	<!-- ********************* End of main-container  **************************** -->


</body>
</html>
