<!DOCTYPE html>
<html>
    <head>
        <title>FamilySunar</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="js/demo.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link href="<?php echo base_url(); ?>css/main.css" type="text/css" rel="stylesheet">
         <link href="<?php echo base_url(); ?>css/register.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <header>
        <div class="logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo.png" class="i"/></a>
        </div>
    </header>