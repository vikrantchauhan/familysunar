<div class="footer">

	<div class="foot">
		<div class="threef">
			<h3>Turn your Passion into a Business</h3>
			<button>
				Open a Shop
			</button>
		</div>

		<div class="threef">
			<h3>
                                SiteMap
                        </h3>
			<div class="div2 div2-long">
				<p>
                                    <a href="">Services</a>
				</p>
				<p>
                                    <a href="">Faqs</a>
				</p>
				<p>
                                    <a href="">Blogs</a>
				</p>
                                <p>
                                    <a href="">Pricing and Commission Info</a>
				</p>
			</div>
			
		</div>
		<div class="threer">
			<h3>
                Follow Us
            </h3>
			<div class="div2">
				<p>
                                    <a href=""><img src="<?php echo base_url(); ?>images/fb.png"/> &nbsp; Facebook</a>
				</p>
				<p>
                                    <a href=""><img src="<?php echo base_url(); ?>images/twitter.png"/> &nbsp; Twitter</a>
				</p>
				<p>
                                    <a href=""><img src="<?php echo base_url(); ?>images/pinterest.png"/> &nbsp; Pinterest</a>
				</p>
			</div>
			<div class="div2">
				<p>
                                    <a href=""><img src="<?php echo base_url(); ?>images/instagram.png"/> &nbsp; Instagram</a>
				</p>
				<p>
                                    <a href=""><img src="<?php echo base_url(); ?>images/youtube.png"/> &nbsp; Youtube</a>
				</p>
				<p>
                                    <a href=""><img src="<?php echo base_url(); ?>images/blogger.png"/> &nbsp; Blogger</a>
				</p>
			</div>
		</div>

	</div>

	<div class="copyright_fs">
            <p>Copyright &copy;2016 FamilySunar.com. All Rights Reserved.</p>
                        <p>
                            <a href="">Terms of Use</a>
			</p>
                        <p>
                            <a href="">Confidential Policy</a>
			</p>
	</div>

		
                        
		
	

</div>

<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

<script>
	$(function() {
		$("#slider-range")
			.slider({
				range: true,
				min: 0,
				max: 50,
				values: [0, 50],
				slide: function(event, ui) {
					$("#amount")
						.val("" + ui.values[0] + " km ");
				}
			});
		$("#amount")
			.val($("#slider-range")
				.slider("values", 0) + " km ");
	});

</script>
<script>
	$(function() {
		var width = $(window)
			.width();
		if(width < 400) {
			$(".rateYo")
				.rateYo({
					maxValue: 1,
					numStars: 5,
					starWidth: "14px"
				});
		}
		if((width > 500) && (width < 800)) {
			$(".rateYo")
				.rateYo({
					maxValue: 1,
					numStars: 5,
					starWidth: "15px"
				});
		}
		else {
			$(".rateYo")
				.rateYo({
					maxValue: 1,
					numStars: 5,
					starWidth: "20px"
				});
		}
	});

</script>

</body>

</html>
