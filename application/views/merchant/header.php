<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="height=device-height, initial-scale=1.0">
	<title>Home Page</title>
        <link href="<?php echo base_url(); ?>css/main.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/cart_page_style.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/merchant_main_page.css" type="text/css" rel="stylesheet">
         
</head>
<body>
    <header>
        <div class="logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo.png" class="i"/></a>
        </div>
 
        <div class="m-reg">
            
            <p>Login</p>
            <input type="email" placeholder="Username" />
            <input type="password" placeholder="Password"/>
            <button>Log In</button>
            <p class="forget">Forgot Password ?</p>
        </div>
    </header>

