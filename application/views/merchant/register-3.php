<div class="user_registration_main-container">
            <section class="fs_section">
                <h2>Complete Your Details</h2>
            </section>
    
            <form class="fs_form_r2" method="POST">
                <div class="fs_le_col">
                    <p class="fs_p">Your Name</p>
                    <input class="fs_input_1" placeholder="Your Name" type="text" name="name">
                </div>
                <div class="fs_ri_col">
                    <p class="fs_p">Email ID</p>
                    <input class="fs_input_1" placeholder="Email ID" type="email" name="email">
                </div>
                <div class="fs_le_col">
                    <p class="fs_p">Tin No.</p>
                    <input class="fs_input_1" placeholder="Tin No." type="text" name="tin_no">
                </div>
                <div class="fs_ri_col">
                    <p class="fs_p">Pan No.</p>
                    <input class="fs_input_1" placeholder="Pan No." type="text" name="pan_no">
                </div>
                <div class="fs_le_col">
                    <button class="file-upload fs_upload">            
                    <input type="file" class="file-input">
                    <img class="fs_image_up" src="<?php echo base_url(); ?>images/upload.png">
                    <span class="fs_up_span">
                    Upload Tin Registartion Image
                    </span>
                    </button>
                </div>
                <div class="fs_ri_col">
                    <button class="file-upload fs_upload">            
                    <input type="file" class="file-input">
                    <img class="fs_image_up" src="<?php echo base_url(); ?>images/upload.png">
                    <span class="fs_up_span">
                    Upload Pan Card Image
                    </span>
                    </button>
                </div>
                <div class="fs_le_col">
                   <p class="fs_p">Shop No.</p>
                    <input class="fs_input_1" placeholder="Shop No." type="text" name="shop_no">
                </div>
                
                <div class="fs_ri_col">
                    <p class="fs_p">Shop Name</p>
                    <input class="fs_input_1" placeholder="Shop Name" type="text" name="shop_name">
                </div>
                
                <div class="clearboth"></div>
                
                <div class="fs_up_col">
                    <p class="fs_p">Address Line 1</p>
                    <input class="fs_input_2" placeholder="Address Line 1" type="text" name="address">
                    <p class="fs_p">Address Line 2</p>
                    <input class="fs_input_2" placeholder="Address Line 2" type="text" name="address">
                </div>
                 <div class="fs_le_col">
                  <p class="fs_p">Landline No.</p>
                    <input class="fs_input_1" placeholder="Landline No." type="tel" name="ln_no">
                </div>
                
                <div class="fs_ri_col">
                    <p class="fs_p">City</p>
                    <input class="fs_input_1" placeholder="City" type="text" name="city">
                </div>
                 <div class="fs_le_col">
                   <p class="fs_p">Landmark</p>
                    <input class="fs_input_1" placeholder="Landmark" type="text" name="landmark">
                </div>
                
                <div class="fs_ri_col">
                    <p class="fs_p">State</p>
                    <input class="fs_input_1" placeholder="State" type="text" name="state">
                </div>
                
                 <div class="fs_le_col">
                    <p class="fs_p">Pin Code</p>
                    <input class="fs_input_1" placeholder="Pin Code" type="text" name="pin_code">
                </div>
                
                <div class="fs_ri_col">
                    <p class="fs_p">Country</p>
                    <input class="fs_input_1" placeholder="Country" type="text" name="Country">
                </div>
                
                
                
                <div class="clearboth"></div>
                <p class="fs_p1">If you have read and agree to the <a href="#"> Terms and Conditions </a>, please continue</p>
                <input  class="fs_submit_1" type="submit" value="Finish"></input>
                
<!--                    <p class="fs_p">Tin No.</p>
                    <input class="fs_input_1" placeholder="Tin No." type="text" name="tin_no">
                    <button class="file-upload fs_upload">            
                    <input type="file" class="file-input">
                    <img class="fs_image_up" src="<?php echo base_url(); ?>images/upload.png">
                    <span class="fs_up_span">
                    Upload Tin Registartion Image
                    </span>
                    </button>
                    <p class="fs_p">Shop Name</p>
                    <input class="fs_input_1" placeholder="Shop Name" type="text" name="shop_name">
                </div>
                <div class="fs_ri_col">
                    <p class="fs_p">Email ID</p>
                    <input class="fs_input_1" placeholder="Email ID" type="email" name="email">
                    <p class="fs_p">Pan No.</p>
                    <input class="fs_input_1" placeholder="Pan No." type="text" name="pan_no">
                    <button class="file-upload fs_upload">            
                    <input type="file" class="file-input">
                    <img class="fs_image_up" src="<?php echo base_url(); ?>images/upload.png">
                    <span class="fs_up_span">
                    Upload Pan Card Image
                    </span>
                    </button>
                    <p class="fs_p">Shop No.</p>
                    <input class="fs_input_1" placeholder="Shop No." type="text" name="shop_no">
                </div>
                <div class="clearboth"></div>
                
                <div class="fs_up_col">
                    <p class="fs_p">Address Line 1</p>
                    <input class="fs_input_2" placeholder="Address Line 1" type="text" name="address">
                    <p class="fs_p">Address Line 2</p>
                    <input class="fs_input_2" placeholder="Address Line 2" type="text" name="address">
                </div>
                <div class="fs_le_col">
                    <p class="fs_p">Landline No.</p>
                    <input class="fs_input_1" placeholder="Landline No." type="tel" name="ln_no">
                    <p class="fs_p">Landmark</p>
                    <input class="fs_input_1" placeholder="Landmark" type="text" name="landmark">
                    <p class="fs_p">Pin Code</p>
                    <input class="fs_input_1" placeholder="Pin Code" type="text" name="pin_code">

                </div>
                <div class="fs_ri_col">
                    <p class="fs_p">City</p>
                    <input class="fs_input_1" placeholder="City" type="text" name="city">
                    <p class="fs_p">State</p>
                    <input class="fs_input_1" placeholder="State" type="text" name="state">
                    <p class="fs_p">Country</p>
                    <input class="fs_input_1" placeholder="Country" type="text" name="Country">
                </div>
                <div class="clearboth"></div>
                <p class="fs_p1">If you have read and agree to the <a href="#"> Terms and Conditions </a>, please continue</p>
                <input  class="fs_submit_1" type="submit" value="Finish"></input>-->

            </form>
        </div>