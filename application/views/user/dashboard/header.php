<?php defined( 'BASEPATH') OR exit( 'No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="height=device-height, initial-scale=1.0">

	<title>Home Page</title>
         
	<link href="<?php echo base_url(); ?>css/main.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/register.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/user_dashboard.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
        <script src="<?php echo base_url(); ?>js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>js/user_dashboard.js"></script>
       

</head>

<body>

    <header class="row">

		<div class="logo">
			<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo.png" class="i"/></a>
		</div>

		<div class="location">
			<div class="city">
				<img src="<?php echo base_url(); ?>images/plane.png" class="plane" />
				<span class="s"></span>
				<select>
					<option value="delhi">Delhi</option>
					<option value="noida">Noida</option>
					<option value="gurgaon">Gurgaon</option>
					<option value="faridabad">Faridabad</option>
				</select>
			</div>

			<div class="search">
				<img src="<?php echo base_url(); ?>images/search-icon.png" />
				<input type="test" placeholder="Search for nearest" />
			</div>

			<Button class="cityS">
				<p>Search</p>
			</Button>

		</div>

		<div class="reg">
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>d_images/notification.gif" /></a>
                    <a href="<?php echo base_url(); ?>familysunar/cartpage"><img src="<?php echo base_url(); ?>images/shopping-bag.png" /></a>
                    
                    <div class="dropdown">
                        <button class="dropbtn">Hi ! Dinesh<span class="arrow-down"></span></button>
                        <div class="dropdown-content">
                          <a href="#">My Profile</a>
                          <a href="#">Order History</a>
                          <a href="#">Review Product</a>
                          <a href="#">Bookmark Product</a>
                          <a href="#">Refer a Friend</a>
                          <a href="#">Logout</a>
                        </div>
                    </div> 
		</div>

	</header>
