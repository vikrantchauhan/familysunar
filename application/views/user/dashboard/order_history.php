<div class="dash-nav">
					
    <ul class="user-nav">
						
        <li class="user-nav-li"><a href="<?php echo base_url(); ?>familysunar/profile_view">Profile</a></li>

        <li class="user-nav-li active"><a href="<?php echo base_url(); ?>familysunar/order_history">Order History</a></li>

        <li class="user-nav-li"><a href="#">Review Product</a></li>

        <li class="user-nav-li"><a href="#">Bookmark Product</a></li>

        <li class="user-nav-li"><a href="<?php echo base_url(); ?>familysunar/refer_friend">Refer your Friend</a></li>


    </ul>
</div>


<!-- **** order container ****-->
<div class="order-container">
   <p class="order-title"> Order History</p>
   <!-- **** history box **** -->
   <div class="history-box">
      <button class="accordion">
         <div class="order-id-box">02545454</div>
         <div class="order-product-name">
            Gold Bangle
            <span class="small-txt">(lorem ipsum)( total items)</span>
         </div>
         <div class="total-sum-box">
            <span>Total Order</span>
            <strong>Rs. 18,011</strong>
         </div>
         <div class="up-arrow">
            <img src="<?php echo base_url(); ?>images/up-arrow.png">
         </div>
         <div class="down-arrow">
            <img src="<?php echo base_url(); ?>images/down-arrow.png">
         </div>
      </button>
      <!-- **** panel box **** -->			
      <div class="panel">
         <!-- **** first row **** -->	 
            <div class="img-box">
                    <img src="<?php echo base_url(); ?>images/bangle_order-history-page.png">
            </div>
            
                     <div class="product-name-box">
                        <p id="product-title"> Gold bangles </p>
                        <p id="product-des">Lorem Ipsum Lorem Ipsum</p>
                     </div>

     
            <!-- **** first column  End **** -->
            <!-- **** second column **** -->	  	
            
               <div class="price-box">
                  <p id="price-box-p">Rs. 18,011</p>
               </div>

            <!-- **** second column end **** -->
            <!-- **** third column **** -->	  	
          
               <div class="order-status">
                  <p id="order-status-p">
                     Delivered on Wed, 30th Jul 2016
                  </p>
               </div>
       
            <!-- **** third column end**** -->
   
         <!-- **** first row **** -->
         <div class="line"></div>
         <!-- **** second row **** -->	 
       
            <!-- **** first column **** --> 	
           
              
                     <div class="img-box">
                        <img src="<?php echo base_url(); ?>images/bangle_order-history-page.png">
                     </div>
                
                 
                     <div class="product-name-box">
                        <p id="product-title"> Gold bangles </p>
                        <p id="product-des">Lorem Ipsum Lorem Ipsum</p>
                     </div>
                 
               
            
            <!-- **** first column  End **** -->
            <!-- **** second column **** -->	  	
           
               <div class="price-box">
                  <p id="price-box-p">
                     Rs. 18,011
                  </p>
               </div>
           
            <!-- **** second column end **** -->
            <!-- **** third column **** -->	  	
            
               <div class="order-status">
                  <p id="order-status-p">
                     Delivered on Wed, 30th Jul 2016
                  </p>
               </div>
           
            <!-- **** third column end**** -->
       
         <!-- **** second row end**** -->
         <div class="line"></div>
         
            
               
                  <div class="seller-name">
                      <span>Seller : </span>
                     <strong>WS Retail</strong>
                  </div>
               
              
                  <div class="order-date">
                     <span>Date : </span>
                     <strong>Sun, 10th Jul 2016</strong>
                  </div>
               
           
            
               <div class="total-sum">
                  <span>Total Order : </span>
                  <strong>Rs. 36,022</strong>
               </div>
            
      
      </div>
      <!-- **** panel box end **** -->
   </div>
   
   <div class="history-box">
      <button class="accordion">
         <div class="order-id-box">02545454</div>
         <div class="order-product-name">
            Gold Bangle
            <span class="small-txt">(lorem ipsum)( total items)</span>
         </div>
         <div class="total-sum-box">
            <span>Total Order</span>
            <strong>Rs. 18,011</strong>
         </div>
         <div class="up-arrow">
            <img src="<?php echo base_url(); ?>images/up-arrow.png">
         </div>
         <div class="down-arrow">
            <img src="<?php echo base_url(); ?>images/down-arrow.png">
         </div>
      </button>
      <!-- **** panel box **** -->			
      <div class="panel">
         <!-- **** first row **** -->	 
            <div class="img-box">
                    <img src="<?php echo base_url(); ?>images/bangle_order-history-page.png">
            </div>
            
                     <div class="product-name-box">
                        <p id="product-title"> Gold bangles </p>
                        <p id="product-des">Lorem Ipsum Lorem Ipsum</p>
                     </div>

     
            <!-- **** first column  End **** -->
            <!-- **** second column **** -->	  	
            
               <div class="price-box">
                  <p id="price-box-p">Rs. 18,011</p>
               </div>

            <!-- **** second column end **** -->
            <!-- **** third column **** -->	  	
          
               <div class="order-status">
                  <p id="order-status-p">
                     Delivered on Wed, 30th Jul 2016
                  </p>
               </div>
       
            <!-- **** third column end**** -->
   
         <!-- **** first row **** -->
         <div class="line"></div>
         <!-- **** second row **** -->	 
       
            <!-- **** first column **** --> 	
           
              
                     <div class="img-box">
                        <img src="<?php echo base_url(); ?>images/bangle_order-history-page.png">
                     </div>
                
                 
                     <div class="product-name-box">
                        <p id="product-title"> Gold bangles </p>
                        <p id="product-des">Lorem Ipsum Lorem Ipsum</p>
                     </div>
                 
               
            
            <!-- **** first column  End **** -->
            <!-- **** second column **** -->	  	
           
               <div class="price-box">
                  <p id="price-box-p">
                     Rs. 18,011
                  </p>
               </div>
           
            <!-- **** second column end **** -->
            <!-- **** third column **** -->	  	
            
               <div class="order-status">
                  <p id="order-status-p">
                     Delivered on Wed, 30th Jul 2016
                  </p>
               </div>
           
            <!-- **** third column end**** -->
       
         <!-- **** second row end**** -->
         <div class="line"></div>
         
            
               
                  <div class="seller-name">
                      <span>Seller : </span>
                     <strong>WS Retail</strong>
                  </div>
               
              
                  <div class="order-date">
                     <span>Date : </span>
                     <strong>Sun, 10th Jul 2016</strong>
                  </div>
               
           
            
               <div class="total-sum">
                  <span>Total Order : </span>
                  <strong>Rs. 36,022</strong>
               </div>
            
      
      </div>
      <!-- **** panel box end **** -->
   </div>
   
   
</div>
<!-- **** history box end**** -->