<div class="dash-nav">
					
    <ul class="user-nav">
						
        <li class="user-nav-li active"><a href="<?php echo base_url(); ?>familysunar/profile_view">Profile</a></li>

        <li class="user-nav-li"><a href="<?php echo base_url(); ?>familysunar/order_history">Order History</a></li>

        <li class="user-nav-li"><a href="#">Review Product</a></li>

        <li class="user-nav-li"><a href="#">Bookmark Product</a></li>

        <li class="user-nav-li"><a href="#">Refer your Friend</a></li>


    </ul>
</div>


<div class="user_registration_main-container">   

        <section class="fs_section">
                <h2>Edit Profile</h2>
                
        </section>
     
		
        <form class="fs_form_r2" method="POST">
                    
                    <div class="fs_le_col">
                        <p class="fs_p">First Name</p>
                        <input class="fs_input_1" placeholder="First Name" type="text" name="fname">
                    </div>
                    <div class="fs_ri_col">
                        <p class="fs_p">Last Name</p>
                        <input class="fs_input_1" placeholder="Last Name" type="text" name="fname">
                    </div>
                    <div class="fs_le_col">
                        <p class="fs_p">Email ID</p>
                        <input class="fs_input_1" placeholder="Email ID" type="email" name="email">
                    </div>
                    <div class="fs_ri_col">
                    <p class="fs_p">Mobile No.</p>
                        <input class="fs_input_1" placeholder="Mobile No." name="number">
                    </div>    
                    <div class="fs_le_col">
                        <p class="fs_p">D.O.B</p>
                        <input class="fs_input_1" placeholder="Optional" name="DOB" id="datepicker">
                    </div>
                     <div class="fs_ri_col">
                         <p class="fs_p">Anniversary</p>
                        <input class="fs_input_1" placeholder="Optional" name="anniversary" id="datepicker2">
                     </div>
                    <div class="fs_le_col">
                        <p class="fs_p">Spouse Birthday</p>
                        <input class="fs_input_1" placeholder="Optional" type="number" name="Birthday">
                    </div>
                    <div class="fs_ri_col">
                       <p class="fs_p">FS Points</p>
                        <input class="fs_input_1" type="text" name="fs-point" placeholder="2000" id="fs-point">
                    </div>
                    
                    <div class="fs_le_col">  
                        <button  class="fs_submit_1 fs_submit_mobile"><img src="<?php echo base_url(); ?>images/save_icon.png"><span>Save</span></button>                    
                    </div>

        </form>
    <div class="clearboth"></div>
        
</div>
