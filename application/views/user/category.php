<div class="cat1">
    
	<div class="sort">
            
		Home/<span class="current">Category</span>
	</div>
    
	<div class="cat">
		Bangles
	</div>
    
	<div class="sort sort1">
		Sort By :
                
		<select>
			<option value="distance">Distance</option>
			<option value="price">Price</option>
			<option value="category">Category</option>
			<option value="rating">Rating</option>
		</select>
	</div>
</div>

<div class="categoryP">
    
	<div class="filters">
            
		<div class="f">
                    
			<div id="filter1"> Rings 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu1">
                                    <p>
                                      <input type="checkbox" id="c1" name="cb">
                                      <label for="c1">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c2" name="cb">
                                      <label for="c2">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c3" name="cb">
                                      <label for="c3">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter2"> Earings 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu2">
				<p>
                                      <input type="checkbox" id="c12" name="cb">
                                      <label for="c12">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c22" name="cb">
                                      <label for="c22">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c32" name="cb">
                                      <label for="c32">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter3"> Pendants <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu3">
				<p>
                                      <input type="checkbox" id="c13" name="cb">
                                      <label for="c13">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c23" name="cb">
                                      <label for="c23">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c33" name="cb">
                                      <label for="c33">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
			<div id="filter4"> Bangles 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu4">
				<p>
                                      <input type="checkbox" id="c14" name="cb">
                                      <label for="c14">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c24" name="cb">
                                      <label for="c24">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c34" name="cb">
                                      <label for="c34">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter5"> Bracelets 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu5">
				<p>
                                      <input type="checkbox" id="c15" name="cb">
                                      <label for="c15">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c25" name="cb">
                                      <label for="c25">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c35" name="cb">
                                      <label for="c35">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter6"> Nose Pins 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu6">
				<p>
                                      <input type="checkbox" id="c16" name="cb">
                                      <label for="c16">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c26" name="cb">
                                      <label for="c26">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c36" name="cb">
                                      <label for="c36">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter7"> Necklace <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu7">
				<p>
                                      <input type="checkbox" id="c17" name="cb">
                                      <label for="c17">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c27" name="cb">
                                      <label for="c27">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c37" name="cb">
                                      <label for="c37">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter8"> Mangalsutra 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu8">
				<p>
                                      <input type="checkbox" id="c18" name="cb">
                                      <label for="c18">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c28" name="cb">
                                      <label for="c28">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c38" name="cb">
                                      <label for="c38">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f">
			<div id="filter9"> Chain 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu9">
				<p>
                                      <input type="checkbox" id="c19" name="cb">
                                      <label for="c19">Option 01</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c29" name="cb">
                                      <label for="c29">Option 02</label>
                                    </p>
                                    <p>
                                      <input type="checkbox" id="c39" name="cb">
                                      <label for="c39">Option 03</label>
                                    </p>
			</div>
		</div>
            
		<div class="f f10">
                    
			<div id="filter10"> Distance Radius 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu10">
                            
				<p>
					<input type="text" id="amount">
				</p>
                                
				<div id="slider-range"></div>
			</div>
		</div>
            
                <div class="f">

                            <div id="filterp"> Price 
                                <span class="plus"> + </span>
                            </div>

                            <div class="hide" id="menup">

                                <p>
					<input type="checkbox" name="ring1"> Rs.5001 To Rs.10000
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Rs.10001 To Rs.20000
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Rs.20001 To Rs.50000 
                                </p>
                                <p>
					<input type="checkbox" name="ring1"> Rs.20001 To Rs.50000 
                                </p>
                                <p>
					<input type="checkbox" name="ring1"> Rs.20001 To Rs.50000 
                                </p>

                                    
                            </div>
                    </div>
            
		<div class="f">
                    
			<div id="filter11"> By Gender 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu11">
				<p>
					<input type="checkbox" name="ring1"> Women 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Men 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Kids 
                                </p>
			</div>
		</div>
            
		<div class="f">
			<div id="filter12"> By Purity 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu12">
				<p>
					<input type="checkbox" name="ring1"> 22K 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> 20K
                                </p>
				<p>
					<input type="checkbox" name="ring1"> 18K 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> 16K 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> 14K 
                                </p>
			</div>
		</div>
            
		<div class="f">
                    
			<div id="filter13"> Metal Type <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu13">
				<p>
					<input type="checkbox" name="ring1"> Only Gold 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Diamond with Gold 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Diamond with Silver 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Only Silver 
                                </p>
			</div>
		</div>
            
		<div class="f">
			<div id="filter14"> Occassion 
                            <span class="plus"> + </span>
			</div>
                    
			<div class="hide" id="menu14">
				<p>
					<input type="checkbox" name="ring1"> Anniversary 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Engagement 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Wedding 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Every Day 
                                </p>
				<p>
					<input type="checkbox" name="ring1"> Work Wear 
                                </p>
			</div>
		</div>
	</div>
	<div class="products">
            
		<div class="about_cat">
			<h4>
                            Bangles
                        </h4>
			<p>
				Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum
			</p>
			<p>
				Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum
			</p>
			<p>
				Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum
			</p>
			<p>
                            <a href="">Read more</a>
			</p>
		</div>
            
		<?php for($i=1;$i<17;$i++) { ?>
		<div class="pro">
                    
			<img src="<?php echo base_url(); ?>images/bookmark.png" class="book">
			<img src="<?php echo base_url(); ?>images/image.png" class="im" />
                        
			<div class="rting">
                            
				<div class="rating">
					<div class="rateYo"></div>
				</div>
			</div>
                        
			<div class="price-pro">
				<span class="cut">
                                    <strike>&#8377; 3232</strike>
                                </span>
				<span class="rate">
                                     &#8377; 2000
                                </span>
				<span class="offer">
                                    ( 25 % off )
                                </span>
			</div>
		</div>
            
		<?php } ?>
            
		<div class="about_cat">
                    
			<h4>
                            Bangles
                        </h4>
                    
			<p>
				Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum
			</p>
			<p>
				Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum
			</p>
			<p>
				Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum Lorsem
				ipsum Lorsem ipsum Lorsem ipsum Lorsem ipsum
			</p>
			<p><a href="">Read more</a>
			</p>
		</div>
	</div>
</div>
