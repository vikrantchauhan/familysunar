<div style="background: #f7f5f6; padding: 2%;">
<div class="user_registration_main-container">
            <section class="fs_section">
                <h2>Register</h2>
            </section>
          
                <form class="fs_form_r2" method="POST">
                    
                    <div class="fs_le_col">
                        <p class="fs_p">First Name</p>
                        <input class="fs_input_1" placeholder="First Name" type="text" name="fname">
                    </div>
                    <div class="fs_ri_col">
                        <p class="fs_p">Last Name</p>
                        <input class="fs_input_1" placeholder="Last Name" type="text" name="fname">
                    </div>
                    <div class="fs_le_col">
                        <p class="fs_p">Email ID</p>
                        <input class="fs_input_1" placeholder="Email ID" type="email" name="email">
                    </div>
                    <div class="fs_ri_col">
                    <p class="fs_p">Mobile No.</p>
                        <input class="fs_input_1" placeholder="Mobile No." name="number">
                    </div>    
                    <div class="fs_le_col">
                        <p class="fs_p">D.O.B</p>
                        <input class="fs_input_1" placeholder="Optional" name="DOB" id="datepicker">
                    </div>
                     <div class="fs_ri_col">
                         <button  class="fs_submit_1 fs_submit_mobile" id="verify-no">Verify Mobile No.</button>
                     </div>
                    <div class="fs_le_col">
                        <p class="fs_p">Spouse Birthday</p>
                        <input class="fs_input_1" placeholder="Optional" name="Birthday" id="datepicker3">
                    </div>
                    <div class="fs_ri_col">
                        <p class="fs_p">Anniversary</p>
                        <input class="fs_input_1" placeholder="Optional" name="anniversary" id="datepicker2">
                    </div>
                    
                    <div class="fs_le_col">  
                       <button  class="fs_submit_1 fs_submit_mobile" id="finish-register">Finish</button>
                    </div>
                 
                </form>
             
                      <div class="clearboth"></div>
        </div>
</div>