<div class="footer">
	<div class="subscribe">
            <div class="subscribe-box">
		<p>
			Get top trends and fresh editor's pick in your inbox with FamilySunar
		</p>
		<input type="email" placeholder="Enter your Email" required>
		<button> Subscribe </button>
            </div>
	</div>
	<div class="foot">
		<div class="threef">
			<h3>Turn your Passion into a Business</h3>
			<button>
				Open a Shop
			</button>
		</div>
		<div class="threef">
			<h3>
            Get to Know Us
         </h3>
			<div class="div2">
				<p><a href="">About Us</a>
				</p>
				<p><a href="">Careers</a>
				</p>
				<p><a href="">Policy and Terms</a>
				</p>
			</div>
			<div class="div2">
				<p><a href="">Cookies Policy</a>
				</p>
				<p><a href="">Contact Us</a>
				</p>
			</div>
		</div>
		<div class="threer">
			<h3>
            Follow Us
         </h3>
			<div class="div2">
				<p><a href=""><img src="<?php echo base_url(); ?>images/fb.png"/> &nbsp; Facebook</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/twitter.png"/> &nbsp; Twitter</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/pinterest.png"/> &nbsp; Pinterest</a>
				</p>
			</div>
			<div class="div2">
				<p><a href=""><img src="<?php echo base_url(); ?>images/instagram.png"/> &nbsp; Instagram</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/youtube.png"/> &nbsp; Youtube</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/blogger.png"/> &nbsp; Blogger</a>
				</p>
			</div>
		</div>
	</div>
	<div class="copyright">
		Copyright &copy;2016 FamilySunar.com. All Rights Reserved.
	</div>
	<div class="last">
		<div class="div3">
			<p class="d1"><img src="<?php echo base_url(); ?>images/globe.png"> India</p>
			<p class="d2">English(UK)</p>
			<p class="d3"> INR </p>
		</div>
		<div class="div4">
			<a href="">
				<p>Help</p>
			</a>
		</div>
	</div>
	<footer>
		<div class="foot4">
			<p><a href="">&copy; FamilySunar.</a>
			</p>
			<p><a href="">Legal</a>
			</p>
			<p><a href="">Privacy</a>
			</p>
			<p><a href="">Interest-Based Ads</a>
			</p>
		</div>
	</footer>
</div>

<script src="<?php echo base_url(); ?>js/category.js"></script>
<script src="<?php echo base_url(); ?>js/homepage.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.rateyo.js"></script>
<script src="<?php echo base_url(); ?>js/register.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/my-rate.js"></script>
<script src="<?php echo base_url(); ?>js/loadmore.js"></script>

<script>
	$(function() {
		$("#slider-range")
			.slider({
				range: true,
				min: 0,
				max: 50,
				values: [5, 50],
				slide: function(event, ui) {
					$("#amount")
						.val("" + ui.values[0] + " km ");
				}
			});
		$("#amount")
			.val($("#slider-range")
				.slider("values", 0) + " km ");
	});

</script>
<script>
$.getJSON('https://freegeoip.net/json/') 
     .done (function(location)
     {
          $('#country').html(location.country_name);
          $('#country_code').html(location.country_code);
          $('#region').html(location.region_name);
          $('#region_code').html(location.region_code);
          $('#city').html(location.city);
          $('#latitude').html(location.latitude);
          $('#longitude').html(location.longitude);
          $('#timezone').html(location.time_zone);
          $('#ip').html(location.ip);
     });
</script>
</body>

</html>
