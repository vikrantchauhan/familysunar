<div class= "main_div"> 
    <div class="about-pro-details">              
    <div class = "image_container" >
        <div class="lg-img">
            <img class = "image" src="<?php echo base_url(); ?>images/image.png"></img>
            <img class = "bookmark" src = "<?php echo base_url(); ?>images/bookmark.png"></img> 
        </div>
                     

        <div class = "rest_images">
             <section class="gallery_1">
                <ul id="flexisel-rest">
                        <li>
                            <img class="rest_img" src="<?php echo base_url(); ?>images/bangel-in-hand.png"></img>
                        </li>
                        <li>
                            <img class="rest_img" src="<?php echo base_url(); ?>images/small_bangel.png"></img>
                        </li>
                        <li>
                            <img class="rest_img" src="<?php echo base_url(); ?>images/small_bangel.png"></img>
                        </li>
                        <li>
                            <img class="rest_img" src="<?php echo base_url(); ?>images/small_bangel.png"></img>
                        </li>
                        <li>
                            <img class="rest_img" src="<?php echo base_url(); ?>images/small_bangel.png"></img>
                        </li>
                                                         
                </ul>    
            </section>

        </div>
	
    </div>    
           

    <div class = "product_details">
        <div class = "product-title"> 
            <p >Atara Twist Sparked Bangle</p>
        </div>
        <div class= "maker_name"> 
            <p >-By Antra Jeweler</p>
        </div>


        <div class="price_info">
            <p class="price"><b>Rs. 29,820</b> 
            <span class="codered">(25%off)</span> 
            <strike>Rs. 31,315</strike> 
            </p>

        </div>

        <div class="com-div">
            <div class = "det-rating">
                <div class="rateYo rateYo_detail" style="dispay: inline-block;">
                </div>
            </div>
            <div class = "rating2"> 
                <img class = "review_pen_image"src="<?php echo base_url(); ?>images/write.png">Write a Review</img>
            </div>
        </div>
        
                   
      

        <div class = "Quality_details">

            <p class = "qualitydetails"> Set in 18 Kt Yellow gold (13.84 gms) with diamonds </p>
            <p class = "qualitydetailss"> (0.42 Ct. IJ-SI) Certified by SGL </p>
            <h3 class = "price_heading"><b>Price Breakup</b></h3>

            <div class="main_breakupbox">	
            
                <div class = "breakup_table">
                    
                        <div class = "td_col">	
                    	    <p> Gold/Silver </p>
                    	    <p>7,421/- </p>
                    	</div>
                    		
                    	<div class = "p">
                    	    <p> + </p>
                    	</div>
                    		
                    	<div class = "td_col">
                    	    <p> Diamond </p>
                    	    <p> 3,421/- </p>
                    	</div>

                    	<div class = "p">
                    	    <p> + </p>
                    	</div>
                    			
                    	<div class = "td_col ex">
                    	    <p> Marking Changes </p>
                    	    <p> 2,421/- </p>
                    	</div>
                    			
                    	<div class = "p">
                    	    <p> + </p>
                    	</div>
                    			
                    	<div class = "td_col ex">
                    	    <p> Taxes and Duties </p>
                    	    <p> 2,421/- </p>
                    	</div>
                    			
                    	<div class = "p">
                    	    <p> + </p>
                    	</div>

                    	<div class = "td_col">
                    	    <p> Others </p>
                    	    <p> 2000/- </p>
                    	</div>
										
                </div>	
	
            </div>
            
        </div>
              
        <div class = "buy_button">
	    <button type="button">Buy Now</button>	
        </div>
	
        <h3 class = "name_values">FAMILYSUNAR PROMISES</h3>
        
        <div class = "fs_values">
                    	
            <div class="guarantee-fs">
                <img src="<?php echo base_url(); ?>images/10days-money-back.png"></img>
                <p>10 days money back</p>
                
            </div>

            <div class="guarantee-fs">
                <img src = "<?php echo base_url(); ?>images/lifetime-exchange.png"></img>
                <p>Lifetime Exchange and buyback</p>
            </div>

            <div class="guarantee-fs">
                <img src = "<?php echo base_url(); ?>images/medal.png"></img>
                <p>Certified Jewellery Only</p>
            </div>

        </div>	
        </div>
    
    </div>
        <div class= "similar_products">
	       <h3 class = "simi">Similar Products</h3>
	</div>
        <section class="gallery_1">
	<ul id="flexisel-sim">
	
		<?php for($i=1;$i<16;$i++) { ?>
                    <li>
		<div class="protest">
			<img src="<?php echo base_url(); ?>images/image.png" class="im" />
			<div class="rating">
				<div class="rateYo"></div>
			</div>
			<div class="price-pro">
				<span class="cut">
            <strike>&#8377; 3232</strike>
            </span>
				<span class="rate">
            &#8377; 2000
            </span>
				<span class="offer">
            ( 25 % off )
            </span>
			</div>
		</div>
                    </li>
		<?php } ?>
                                                         
	</ul>    
</section>

       <div class = "prod_description">
        	
           <div class = "product_des" >
               
	       <table class = "table_1">

                   <p class = "plus_sign_red">+</p>  
                   <h3 class="h3-hed"> PRODUCT DETAILS </h3>


        	   <tr>
        	   
                       <td class="det-title"> Height</td>
                       <td class="det-value"> - 50.9 mm </td>
        	           
		   </tr>
        			
        	   <tr>
        	   
		       <td class="det-title"> Width</td>
                       <td class="det-value"> - 7.69 mm </td>
        			
                   </tr>

        	   <tr>

        	       <td class="det-title"> Net Weight (in gm)</td>
                       <td class="det-value"> - 16.24 gm </td>
        	   </tr>
        			
        	</table>

        	<table class = "table_1">

        	    <p class = "plus_sign_red">+</p>
                <h3 class="h3-hed"> METAL DETAILS </h3>

        	    <tr>
        	        
        		<td class="det-title"> Karat  </td>
                        <td class="det-value"> - 0.520 Ct </td>
        		
                    </tr>
        			
        	    <tr>
        		<td class="det-title"> Type </td>
                        <td class="det-value"> - 18 Kt Yellow Gold  </td>
        	    </tr>
        			
        	    <tr>
        		<td class="det-title"> Weight </td>
                        <td class="det-value"> - 16.14 gms </td>
        	    </tr>
        			
        	</table>

        	<table class = "table_1">
                <p class = "plus_sign_red">+</p>
        	    <h3 class="h3-hed"> DIAMOND DETAILS </h3>
                     
        	    <tr> 
        		<td class="det-title"> Total Weight </td>
                        <td class="det-value"> - 16.24 gm </td>
        	    </tr>
        			
        	    <tr>
        		<td class="det-title"> Total No. of Diamonds </td>
        		<td class="det-value"> - three </td>
                    </tr>
        		
		    <tr> 
			<td class="det-title"> Net Weight (in gm)</td>
                        <td class="det-value"> - 16.24 gm </td>
        	    </tr>	


        	</table>	

            </div>	

            <div class = "customer_form">
                <h3 class="h3-hed"> HAVE A QUESTION </h3>
        	<h3 class="h3-hed1"> Call Us At : 9999999999 </h3>
        	
	        <div class = "main_form">
	
        	 
        	        <form>
      	
                            <p class="form-title">Name</p>
                            <p><input type="text" name="firstname"></p><br>
      
                            <p class="form-title">Email</p>
                            <p><input type="text" name="email"></p><br>

                            <p class="form-title">Phone No</p>
                            <p><input type="text" name="Phone No."></p><br>

                            <p class="form-title">Query</p>
                            <p><input type="text" name="Query"></p><br>	

                            <button type="button">Submit</button>

    			</form>	

	
        	</div>



        	<div class = "customer_support_number">
        	    <h3 class="h3-hed"> CUSTOMER SUPPORT NUMBER</h3>
        	    <div>
                    <img src = "<?php echo base_url(); ?>images/phone.png">
                    <b class = "cust_number_help"> 3215646146123131 </b>
                </div>    

                <div class= "refund_div">
        	        <img src = "<?php echo base_url(); ?>images/return-&-refund.png"> 
                    <a class="return"href="#"> Return/Refund Policy </a>
        		</div>
            </div>	




        </div>
	
     </div>



        <div class="work-info">
       		<H4 class="how-we-work"> HOW WE WORK </H4>
        <div class="col1-export-jewl">
            
	    <div class="col1-img">
                <img src="<?php echo base_url(); ?>images/explore-jewellery.png" width="100%" alt="explore-jewellery">
            </div>

            <div class="col1-title">
                <p> Explore Jewellery</p>
            </div>

        </div>

        <div class="col2-book-jwel">

            <div class="col2-img">
                <img src="<?php echo base_url(); ?>images/book-jewellery.png" width="100%" alt="book-jewellery" >
            </div>

            <div class="col2-title">
                <p>Book Jewellery</p>
            </div>

       </div>

       <div class="col3-pick-jwel">
            <div class="col3-img">
                <img src="<?php echo base_url(); ?>images/pick-near-by.png" width="100%" alt="pick-near-by">
            </div>

            <div class="col3-title">
                <p> Pick Near By</p>
            </div>

       </div>

   </div>




        	
    	<div class="review-info">
    	    <div class ="top_reviews">

    	        <H4 class="review_text"> TOP REVIEWS </H4>
    		<button class="review_button" type="button">Write a Review</button>

    	    </div>
            
            <div class="row-review">
                
                <div class="review-breif">
                    <p class="rateYo"></p>
                    <p>Dinesh Dogra</p>
                    <p>7-Jan-2016</p>
                </div>
                
                <div class="review-say">
                    <h4>Best Choices for Bangels</h4>
                    <p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>
                </div>
           
            </div>
            
             <div class="row-review">
                
                <div class="review-breif">
                    <p class="rateYo"></p>
                    <p>Dinesh Dogra</p>
                    <p>7-Jan-2016</p>
                </div>
                
                <div class="review-say">
                    <h4>Best Choices for Bangels</h4>
                    <p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>
                </div>
           
            </div>
            
            <!--<table class = "review_table">

    	    <tr>
    				
                <td class="rate-t">
    		    <div class = "rating">
                        <div class="rateYo">
			</div>

                    </div>
		<td class = "review_td">
                    <p>Dinesh Dogra</p>
                    <p>7-Jan-2016</p>
    		</td>

    		<td>
    		    <p><b>Best Choices for Bangels</b></p>

    		    <p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
    		</td>

            </tr>  

            <tr>
    				
    		<td class="rate-t">
    		    <div class = "rating">

                        <div class="rateYo">
			</div>

                    </div>
		<td>
                    <p>Dinesh Dogra</p>
                    <p>7-Jan-2016</p>
    		</td>

    		<td>
    		    <p><b>Best Choices for Bangels</b></p>

    		    <p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
    		</td>

            </tr>

            <tr>
    				
    		<td>
    		    <div class = "rating">

                        <div class="rateYo">
			</div>

                    </div>

                <td>
                    <p>Dinesh Dogra</p>
                    <p>7-Jan-2016</p>
    			
                </td>

    		<td>
    		    <p><b>Best Choices for Bangels</b></p>
                    
    		    <p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
    		</td>

            </tr>

        </table>  --> 
		
    	</div>	

    <div style="text-align: center;">	 
        <button class="view_more_button" type="button">View More</button>	
    </div>    
          </div>  
