
<!-- ********************* Start of main container ******************  -->
<div class="cart-page-main-body">
   
   <div class="shopping-bag">
      <p> Shopping Bag</p>
   </div>

<!-- ********************* start of cart container ******************  -->   
  
   <div class="cart-container">
   
<!-- ********************* start of cart box ******************  -->      
      <div class="cart-box">
      
         <div class="cross-img">
					
			<a href="#"><img src="<?php echo base_url(); ?>images/close_2.png"></a>
		
		</div>   
         
         <p class="box-title"> 3 item successfully added to your cart</p>
 <!-- ********************* 1st cart box item ******************  -->      
         <div class="box-item">
            
            <div class="item-img">
              
               <a href="#"><img src="<?php echo base_url(); ?>images/bangel.png" alt="item-img-description"></a>
            
            </div>
            
            <div class="item-description">
               
               <p class="item-name"> The Melia Bengal</p>
              
               <div class="quantity">
                 
                  <p id="quantity">Quantity</p>
                 
                  <div class="dropdown-select">
                     
                     <select id="select-quantity1">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    
                     </select>
                 
                  </div>
              
               </div>
               
               <div class="pro-description">
                  
                  <p class="heading"> size</p> 
                  <p class="hed-des">:&nbsp; &nbsp;6</p>
               
               </div>
              
               <div class="pro-description">
                  
                  <p class="heading">Metal type</p>
                  <p class="hed-des">:&nbsp; &nbsp;18KT Yellow Gold</p>
               
               </div>
             
               <div class="pro-description">
                  
                  <p class="heading">Metal Weight</p>
                  <p class="hed-des">:&nbsp; &nbsp;3.15GMS</p>
              
               </div>
               
               <div class="pro-description">
                  
                  <p class="heading">Stone</p>
                  <p class="hed-des">:&nbsp; &nbsp;amethyst</p>
              
               </div>
              
               <div class="pro-description">
                 
                  <p class="heading">No. of stones</p>
                  <p class="hed-des">:&nbsp; &nbsp;1</p>
               
               </div>
               
               <div class="item-price">
                 
                  <p class="item-price-des" id="item1-price">Rs. 9099</p>
               
               </div>
               
               <div class="small-cross-img">
							
					<a href="#"><img src="<?php echo base_url(); ?>images/small_close.png"></a>
						
				</div>
            
            </div>
           
            <div class="amount-payable">
               
               <p> Amount Payable</p>
               <p class="amount"> Rs.<span id="price1">9099</span> </p>
           
            </div>
        
         </div> <!-- *****End of 1st cart box item *****  -->
        
         <div class="hr-line"></div>
     
   <!-- ********************* 2nd cart box item ******************  -->  
         <div class="box-item">
           
            <div class="item-img">
               
               <a href="#"><img src="<?php echo base_url(); ?>images/bangel.png" alt="item-img-description"></a>
            
            </div>
          
            <div class="item-description">
             
               <p class="item-name"> The Melia Bengal</p>
               
               <div class="quantity">
                
                  <p id="quantity">Quantity</p>
                 
                  <div class="dropdown-select">
                     
                     <select id="select-quantity2">
                        <option value="1">1</option>
                        <option value="1">2</option>
                        <option value="1">3</option>
                        <option value="1">4</option>
                     </select>
                 
                  </div>
              
               </div>
               
               <div class="size">
                  
                  <p class="heading"> size</p>
                  <p class="hed-des">:&nbsp; &nbsp;6</p>
              
               </div>
              
               <div class="metal-type">
                 
                  <p class="heading">Metal type</p>
                  <p class="hed-des">:&nbsp; &nbsp;18KT Yellow Gold</p>
               
               </div>
              
               <div class="metal-weight">
                  <p class="heading">Metal Weight</p>
                  <p class="hed-des">:&nbsp; &nbsp;3.15GMS</p>
               </div>
               
               <div class="stone-type">
                 
                  <p class="heading">Stone</p>
                  <p class="hed-des">:&nbsp; &nbsp;amethyst</p>
              
               </div>
               <div class="no-of-stone">
                  <p class="heading">No. of stones</p>
                  <p class="hed-des">:&nbsp; &nbsp;1</p>
               </div>
              
               <div class="item-price">
                 
                  <p class="item-price-des" id="item2-price">Rs. 9099</p>
              
               </div>
               
               <div class="small-cross-img">
							
					<a href="#"><img src="<?php echo base_url(); ?>images/small_close.png"></a>
						
				</div>
            
            </div>
            
            <div class="amount-payable">
               
               <p> Amount Payable</p>
               <p class="amount"> Rs.<span id="price2">9099</span></p>
           
            </div>
        
         </div><!-- ***** End of 2nd cart box item *****  -->
        
         <div class="hr-line"></div>
         
  <!-- ***** 3rd cart box item *****  -->     
         <div class="box-item">
            
            <div class="item-img">
              
               <a href="#"><img src="<?php echo base_url(); ?>images/bangel.png" alt="item-img-description"></a>
           
            </div>
           
            <div class="item-description">
             
               <p class="item-name"> The Melia Bengal</p>
              
               <div class="quantity">
                 
                  <p id="quantity">Quantity</p>
                 
                  <div class="dropdown-select">
                   
                     <select id="select-quantity3">
                        <option value="1">1</option>
                        <option value="1">2</option>
                        <option value="1">3</option>
                        <option value="1">4</option>
                     </select>
                 
                  </div>
              
               </div>
             
               <div class="size">
                  
                  <p class="heading"> size</p>
                  <p class="hed-des">:&nbsp; &nbsp;6</p>
              
               </div>
               
               <div class="metal-type">
                  
                  <p class="heading">Metal type</p>
                  <p class="hed-des">:&nbsp; &nbsp;18KT Yellow Gold</p>
             
               </div>
              
               <div class="metal-weight">
                  
                  <p class="heading">Metal Weight</p>
                  <p class="hed-des">:&nbsp; &nbsp;3.15GMS</p>
              
               </div>
              
               <div class="stone-type">
                 
                  <p class="heading">Stone</p>
                  <p class="hed-des">:&nbsp; &nbsp;amethyst</p>
              
               </div>
             
               <div class="no-of-stone">
                  
                  <p class="heading">No. of stones</p>
                  <p class="hed-des">:&nbsp; &nbsp;1</p>
               
               </div>
              
               <div class="item-price">
                 
                  <p class="item-price-des" id="item3-price">Rs. 9099</p>
              
               </div>
               
               <div class="small-cross-img">
							
					<a href="#"><img src="<?php echo base_url(); ?>images/small_close.png"></a>
						
				</div>
         
            </div>
            
            <div class="amount-payable">
               
               <p> Amount Payable</p>
              
               <p class="amount"> Rs. <span id="price3">9099</span></p>
          
            </div>
        
         </div> <!-- ***** End of 3rd cart box item *****  -->
         
         <div class="coupon-btn">
           
            <div class="btn-box">
            
               <input type="button" name="redeam-btn" value="Redeem FS Point">
               <input type="button" name="aplly-btn" value="Apply coupons">
            
            </div>
           
            <div class="total-amount">
              
               <p id="total-amnt">Total:</p>
               <p id="amnt-digit">&nbsp; &nbsp;<span id="total-item-price">&#8377;27297</span></p>
           
            </div>
        
         </div>
     
      </div><!-- ******* End of cart box ********  -->
  
   </div><!-- ***** End of cart container *****  -->	
   
   
<!-- *********************  Start of checkout button box ******************  -->

   <div class="checkout-btn">
     
      <div class="continue-shop">
        
         <input type="button" name="redeam-btn" value="Continue Shopping">
      
      </div>
     
      <div class="place-order">
         
         <input type="button" name="redeam-btn" value="Place Booking">
     
      </div>
   
   </div> <!-- ***** End of checkout button box *****  -->
  
 
 <!-- ***** Start of work container ***** -->  
   <div class="work-info-cart">
       		<H4 class="how-we-work"> HOW WE WORK </H4>
        <div class="col1-export-jewl">
            
	    <div class="col1-img">
                <img src="<?php echo base_url(); ?>images/explore-jewellery.png" width="100%" alt="explore-jewellery">
            </div>

            <div class="col1-title">
                <p> Explore Jewellery</p>
            </div>

        </div>

        <div class="col2-book-jwel">

            <div class="col2-img">
                <img src="<?php echo base_url(); ?>images/book-jewellery.png" width="100%" alt="book-jewellery" >
            </div>

            <div class="col2-title">
                <p>Book Jewellery</p>
            </div>

       </div>

       <div class="col3-pick-jwel">
            <div class="col3-img">
                <img src="<?php echo base_url(); ?>images/pick-near-by.png" width="100%" alt="pick-near-by">
            </div>

            <div class="col3-title">
                <p> Pick Near By</p>
            </div>

       </div>

   </div><!-- ***** end of work container *****  -->		

</div><!-- ***** End of main container *****  -->

