<?php defined( 'BASEPATH') OR exit( 'No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="height=device-height, initial-scale=1.0">

	<title>Home Page</title>
         
	<link href="<?php echo base_url(); ?>css/main.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/category.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/detailed_page.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/cart_page_style.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>css/jquery.rateyo.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/register.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-customselect.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
        <script src="<?php echo base_url(); ?>js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.flexisel.js"></script>
        <script src="<?php echo base_url(); ?>js/product_slider.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-customselect.js"></script>
                <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.datepick.css"> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.plugin.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.datepick.js"></script>
        

</head>

<body>

    <header>
        

		<div class="logo">
			<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>images/logo.png" class="i"/></a>
		</div>

		<div class="location">
			<div class="city">
				<img src="<?php echo base_url(); ?>images/plane.png" class="plane" />
                                <img src="<?php echo base_url(); ?>images/down-arrow_2.png" class="down" />
				<select id='standard' name='standard' class='custom-select'>
                                    <option value="" >Select City</option>
                                    <option value='1'>Delhi</option>
                                    <option value='2'>Noida</option>
                                    <option value='3'>Gurgaon</option>
                                    <option value='4'>Gaziabad</option>
                                    <option value='5'>Faridabad</option>
                                    <option value='6'>Ahmedabad</option> 
                                    <option value='8'>Chandigarh</option>
                                    <option value='9'>Chennai</option>
                                   

                                </select>
                                
			</div>

			<div class="search">
				<img src="<?php echo base_url(); ?>images/search-icon.png" />
				<input type="test" placeholder="Search for nearest" />
			</div>

			<Button class="cityS">
				Search
			</Button>

		</div>

		<div class="reg">
			<a href="<?php echo base_url(); ?>familysunar/register" class="r">REGISTER</a>&nbsp;&#124;
			<a href="<?php echo base_url(); ?>familysunar/login" class="l">LOGIN</a>&nbsp; &#124;
			<a href="<?php echo base_url(); ?>familysunar/cartpage"><img src="<?php echo base_url(); ?>images/shopping-bag.png" /></a>
		</div>

	</header>
