<div class="footer">
	
	<div class="foot">
		<div class="threef">
			<h3>Turn your Passion into a Business</h3>
			<button>
				Open a Shop
			</button>
		</div>
		<div class="threef">
			<h3>
            Get to Know Us
         </h3>
			<div class="div2">
				<p><a href="">About Us</a>
				</p>
				<p><a href="">Careers</a>
				</p>
				<p><a href="">Policy and Terms</a>
				</p>
			</div>
			<div class="div2">
				<p><a href="">Cookies Policy</a>
				</p>
				<p><a href="">Contact Us</a>
				</p>
			</div>
		</div>
		<div class="threer">
			<h3>
            Follow Us
         </h3>
			<div class="div2">
				<p><a href=""><img src="<?php echo base_url(); ?>images/fb.png"/> &nbsp; Facebook</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/twitter.png"/> &nbsp; Twitter</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/pinterest.png"/> &nbsp; Pinterest</a>
				</p>
			</div>
			<div class="div2">
				<p><a href=""><img src="<?php echo base_url(); ?>images/instagram.png"/> &nbsp; Instagram</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/youtube.png"/> &nbsp; Youtube</a>
				</p>
				<p><a href=""><img src="<?php echo base_url(); ?>images/blogger.png"/> &nbsp; Blogger</a>
				</p>
			</div>
		</div>
	</div>
	<div class="copyright">
		Copyright &copy;2016 FamilySunar.com. All Rights Reserved.
	</div>
	<div class="last">
		<div class="div3">
			<p class="d1"><img src="<?php echo base_url(); ?>images/globe.png"> India</p>
			<p class="d2">English(UK)</p>
			<p class="d3"> INR </p>
		</div>
		<div class="div4">
			<a href="">
				<p>Help</p>
			</a>
		</div>
	</div>
	<footer>
		<div class="foot4">
			<p><a href="">&copy; FamilySunar.</a>
			</p>
			<p><a href="">Legal</a>
			</p>
			<p><a href="">Privacy</a>
			</p>
			<p><a href="">Interest-Based Ads</a>
			</p>
		</div>
	</footer>
</div>

<script src="<?php echo base_url(); ?>js/category.js"></script>
<script src="<?php echo base_url(); ?>js/homepage.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.rateyo.js"></script>
<script src="<?php echo base_url(); ?>js/register.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>

<script>
	$(function() {
		$("#slider-range")
			.slider({
				range: true,
				min: 0,
				max: 50,
				values: [5, 50],
				slide: function(event, ui) {
					$("#amount")
						.val("" + ui.values[0] + " km ");
				}
			});
		$("#amount")
			.val($("#slider-range")
				.slider("values", 0) + " km ");
	});

</script>


        
<script>
              
 $(function() {
  $("#standard").customselect();
});

	$(function() {
		var width = $(window)
			.width();
		if(width < 500) {
			$(".rateYo")
				.rateYo({
					maxValue: 1,
					numStars: 5,
					starWidth: "18px"
				});
		}
		if((width > 500) && (width < 800)) {
			$(".rateYo")
				.rateYo({
					maxValue: 1,
					numStars: 5,
					starWidth: "20px"
				});
		}
		else {
			$(".rateYo")
				.rateYo({
					maxValue: 1,
					numStars: 5,
					starWidth: "20px"
				});
		}
	});

</script>
</body>

</html>

