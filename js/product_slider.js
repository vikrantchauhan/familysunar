

		$(window).load(function() {

			$("#flexisel1").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                        $("#flexisel2").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                        $("#flexisel3").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                        $("#flexisel4").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                        $("#flexisel5").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                        $("#flexisel6").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                        $("#flexisel7").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                         $("#flexisel-banner").flexisel({
				visibleItems: 1,
				animationSpeed: 800,
				autoPlay: true,
				autoPlaySpeed: 2000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: false
			
			});
                        
                        $("#flexisel-sim").flexisel({
				visibleItems: 5,
				animationSpeed: 400,
				autoPlay: true,
				autoPlaySpeed: 50000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
                                    mobile: {
                                        changePoint:500,
                                        visibleItems: 1    
                                    },
				    portrait: { 
				        changePoint:500,
				        visibleItems: 1
				    }, 
				    landscape: { 
				        changePoint:900,
				        visibleItems: 2
				    },
				    tablet: { 
				        changePoint:1200,
				        visibleItems: 3
				    },
                                    medium: { 
				        changePoint:1400,
				        visibleItems: 4
				    }
				}
			});
                         $("#flexisel-rest").flexisel({
				visibleItems: 5,
				animationSpeed: 800,
				autoPlay: true,
				autoPlaySpeed: 200000,            
				pauseOnHover: true,
				enableResponsiveBreakpoints: false
			
			});
		});


